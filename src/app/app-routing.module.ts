import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseProjectComponent } from 'projects/base-project/src/public-api';
import { SecurityAuthGuard } from './security/security.authguard';


const routes: Routes = [
  {
    path: 'base-project',
    component: BaseProjectComponent,
    canActivate: [SecurityAuthGuard],

  },
  {
    path: '',
    redirectTo: 'base-project',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
