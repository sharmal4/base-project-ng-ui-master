# DmsTraining

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0.

## Renaming the project and root component names according to your own project name

Under base-project-ui/projects folder you should have your project folder.
Currently the project named as 'base-project'.
You should rename the above project folder name with your new project name.
Eg: If you new project name is 'gadget-new-project-ui' then replace 'base-project' with 'gadget-new-project'

**Note:** Once you change the project name from 'base-project' to your new project name
1. Rename the other file names which contains 'base-project' to your new project name under base-project-ui/projects/base-project/src/lib folder.
    Eg: If your new project name is 'gadget-new-project-ui' then rename 'base-project.component.html' to 'gadget-new-project.component.html'
2. Use Find & Replace feature in your editor to replace all the occurrences of 'base-project' with your new project name.
3. Component, Module and Service calss names should renamed according to your new project name. (Use Find & Replace feature to do the replacement)
    Eg: If your new project name is 'gadget-new-project-ui' then rename the 'BaseProjectModule' to 'NewProjectModule'. (Replace the all occurrences)
        (Do the same for Component and Service classes)

## Install npm dependencies

Run `npm install` after navigated to the base-project-ui root folder.

## Build the project

Run `npm run build` after successfully installed the npm packages.

## Development server

Run `npm start` to deploy the app in your local server. Navigate to `http://localhost:4510/`. The app will automatically reload if you change any of the source files.
