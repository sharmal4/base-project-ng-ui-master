import { Component } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/card";
export class BaseProjectComponent {
}
BaseProjectComponent.ɵfac = function BaseProjectComponent_Factory(t) { return new (t || BaseProjectComponent)(); };
BaseProjectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: BaseProjectComponent, selectors: [["lib-base-project"]], decls: 4, vars: 0, template: function BaseProjectComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-card");
        i0.ɵɵelementStart(1, "mat-card-header");
        i0.ɵɵelementStart(2, "mat-card-title");
        i0.ɵɵtext(3, "Hello Training Team");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } }, directives: [i1.MatCard, i1.MatCardHeader, i1.MatCardTitle], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(BaseProjectComponent, [{
        type: Component,
        args: [{
                selector: 'lib-base-project',
                templateUrl: './base-project.component.html',
                styleUrls: ['./base-project.component.scss'],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1wcm9qZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL2Jhc2UtcHJvamVjdC9zcmMvbGliL2Jhc2UtcHJvamVjdC5jb21wb25lbnQudHMiLCIuLi8uLi8uLi8uLi9wcm9qZWN0cy9iYXNlLXByb2plY3Qvc3JjL2xpYi9iYXNlLXByb2plY3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBNkIsTUFBTSxlQUFlLENBQUM7OztBQU9yRSxNQUFNLE9BQU8sb0JBQW9COzt3RkFBcEIsb0JBQW9CO3lEQUFwQixvQkFBb0I7UUNQakMsZ0NBQ0U7UUFBQSx1Q0FDRTtRQUFBLHNDQUFnQjtRQUFBLG1DQUFtQjtRQUFBLGlCQUFpQjtRQUN0RCxpQkFBa0I7UUFDcEIsaUJBQVc7O2tEREdFLG9CQUFvQjtjQUxoQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsV0FBVyxFQUFFLCtCQUErQjtnQkFDNUMsU0FBUyxFQUFFLENBQUMsK0JBQStCLENBQUM7YUFDN0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCwgVmlld0NoaWxkLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWJhc2UtcHJvamVjdCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9iYXNlLXByb2plY3QuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9iYXNlLXByb2plY3QuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgQmFzZVByb2plY3RDb21wb25lbnQge1xuXG59XG4iLCI8bWF0LWNhcmQ+XG4gIDxtYXQtY2FyZC1oZWFkZXI+XG4gICAgPG1hdC1jYXJkLXRpdGxlPkhlbGxvIFRyYWluaW5nIFRlYW08L21hdC1jYXJkLXRpdGxlPlxuICA8L21hdC1jYXJkLWhlYWRlcj5cbjwvbWF0LWNhcmQ+XG4iXX0=