import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { BaseProjectComponent } from './base-project.component';
import { RouterModule } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
export class BaseProjectModule {
}
BaseProjectModule.ɵmod = i0.ɵɵdefineNgModule({ type: BaseProjectModule });
BaseProjectModule.ɵinj = i0.ɵɵdefineInjector({ factory: function BaseProjectModule_Factory(t) { return new (t || BaseProjectModule)(); }, providers: [{
            provide: 'base-project',
            useValue: BaseProjectComponent
        }], imports: [[
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MatCardModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            MatDividerModule,
            MatListModule,
            MatStepperModule,
            MatProgressBarModule,
            MatCheckboxModule,
            MatIconModule,
            MatSelectModule,
            MatDatepickerModule,
            MatSnackBarModule,
            MatTableModule,
            MatPaginatorModule,
            FlexLayoutModule,
            HttpClientModule,
            MatTooltipModule,
            RouterModule.forChild([
                {
                    path: '', pathMatch: 'full', component: BaseProjectComponent
                }
            ])
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(BaseProjectModule, { declarations: [BaseProjectComponent], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        MatDividerModule,
        MatListModule,
        MatStepperModule,
        MatProgressBarModule,
        MatCheckboxModule,
        MatIconModule,
        MatSelectModule,
        MatDatepickerModule,
        MatSnackBarModule,
        MatTableModule,
        MatPaginatorModule,
        FlexLayoutModule,
        HttpClientModule,
        MatTooltipModule, i1.RouterModule], exports: [BaseProjectComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(BaseProjectModule, [{
        type: NgModule,
        args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatCardModule,
                    MatInputModule,
                    MatFormFieldModule,
                    MatButtonModule,
                    MatDividerModule,
                    MatListModule,
                    MatStepperModule,
                    MatProgressBarModule,
                    MatCheckboxModule,
                    MatIconModule,
                    MatSelectModule,
                    MatDatepickerModule,
                    MatSnackBarModule,
                    MatTableModule,
                    MatPaginatorModule,
                    FlexLayoutModule,
                    HttpClientModule,
                    MatTooltipModule,
                    RouterModule.forChild([
                        {
                            path: '', pathMatch: 'full', component: BaseProjectComponent
                        }
                    ])
                ],
                declarations: [
                    BaseProjectComponent
                ],
                providers: [{
                        provide: 'base-project',
                        useValue: BaseProjectComponent
                    }],
                exports: [
                    BaseProjectComponent
                ],
                entryComponents: [
                    BaseProjectComponent
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1wcm9qZWN0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL2Jhc2UtcHJvamVjdC9zcmMvbGliL2Jhc2UtcHJvamVjdC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ25FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzdELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7OztBQWlEN0QsTUFBTSxPQUFPLGlCQUFpQjs7cURBQWpCLGlCQUFpQjtpSEFBakIsaUJBQWlCLG1CQVoxQixDQUFDO1lBQ0MsT0FBTyxFQUFFLGNBQWM7WUFDdkIsUUFBUSxFQUFFLG9CQUFvQjtTQUMvQixDQUFDLFlBcENGO1lBQ0UsWUFBWTtZQUNaLFdBQVc7WUFDWCxtQkFBbUI7WUFDbkIsYUFBYTtZQUNiLGNBQWM7WUFDZCxrQkFBa0I7WUFDbEIsZUFBZTtZQUNmLGdCQUFnQjtZQUNoQixhQUFhO1lBQ2IsZ0JBQWdCO1lBQ2hCLG9CQUFvQjtZQUNwQixpQkFBaUI7WUFDakIsYUFBYTtZQUNiLGVBQWU7WUFDZixtQkFBbUI7WUFDbkIsaUJBQWlCO1lBQ2pCLGNBQWM7WUFDZCxrQkFBa0I7WUFDbEIsZ0JBQWdCO1lBQ2hCLGdCQUFnQjtZQUNoQixnQkFBZ0I7WUFDaEIsWUFBWSxDQUFDLFFBQVEsQ0FBQztnQkFDcEI7b0JBQ0UsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxvQkFBb0I7aUJBQzdEO2FBQ0YsQ0FBQztTQUNIO3dGQWtCUSxpQkFBaUIsbUJBZnhCLG9CQUFvQixhQTdCcEIsWUFBWTtRQUNaLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLGNBQWM7UUFDZCxrQkFBa0I7UUFDbEIsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsZ0JBQWdCO1FBQ2hCLG9CQUFvQjtRQUNwQixpQkFBaUI7UUFDakIsYUFBYTtRQUNiLGVBQWU7UUFDZixtQkFBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLGNBQWM7UUFDZCxrQkFBa0I7UUFDbEIsZ0JBQWdCO1FBQ2hCLGdCQUFnQjtRQUNoQixnQkFBZ0IsOEJBa0JoQixvQkFBb0I7a0RBTWIsaUJBQWlCO2NBL0M3QixRQUFRO2VBQUM7Z0JBQ1IsT0FBTyxFQUNMO29CQUNFLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGFBQWE7b0JBQ2IsY0FBYztvQkFDZCxrQkFBa0I7b0JBQ2xCLGVBQWU7b0JBQ2YsZ0JBQWdCO29CQUNoQixhQUFhO29CQUNiLGdCQUFnQjtvQkFDaEIsb0JBQW9CO29CQUNwQixpQkFBaUI7b0JBQ2pCLGFBQWE7b0JBQ2IsZUFBZTtvQkFDZixtQkFBbUI7b0JBQ25CLGlCQUFpQjtvQkFDakIsY0FBYztvQkFDZCxrQkFBa0I7b0JBQ2xCLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLFlBQVksQ0FBQyxRQUFRLENBQUM7d0JBQ3BCOzRCQUNFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsb0JBQW9CO3lCQUM3RDtxQkFDRixDQUFDO2lCQUNIO2dCQUNILFlBQVksRUFDVjtvQkFDRSxvQkFBb0I7aUJBQ3JCO2dCQUNILFNBQVMsRUFDUCxDQUFDO3dCQUNDLE9BQU8sRUFBRSxjQUFjO3dCQUN2QixRQUFRLEVBQUUsb0JBQW9CO3FCQUMvQixDQUFDO2dCQUNKLE9BQU8sRUFDTDtvQkFDRSxvQkFBb0I7aUJBQ3JCO2dCQUNILGVBQWUsRUFBRTtvQkFDZixvQkFBb0I7aUJBQ3JCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0QnV0dG9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYnV0dG9uJztcbmltcG9ydCB7IE1hdENhcmRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jYXJkJztcbmltcG9ydCB7IE1hdENoZWNrYm94TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hlY2tib3gnO1xuaW1wb3J0IHsgTWF0RGF0ZXBpY2tlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RhdGVwaWNrZXInO1xuaW1wb3J0IHsgTWF0RGl2aWRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpdmlkZXInO1xuaW1wb3J0IHsgTWF0Rm9ybUZpZWxkTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZm9ybS1maWVsZCc7XG5pbXBvcnQgeyBNYXRJY29uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaWNvbic7XG5pbXBvcnQgeyBNYXRJbnB1dE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2lucHV0JztcbmltcG9ydCB7IE1hdExpc3RNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9saXN0JztcbmltcG9ydCB7IE1hdFBhZ2luYXRvck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3BhZ2luYXRvcic7XG5pbXBvcnQgeyBNYXRQcm9ncmVzc0Jhck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3Byb2dyZXNzLWJhcic7XG5pbXBvcnQgeyBNYXRTZWxlY3RNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zZWxlY3QnO1xuaW1wb3J0IHsgTWF0U25hY2tCYXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXInO1xuaW1wb3J0IHsgTWF0U3RlcHBlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3N0ZXBwZXInO1xuaW1wb3J0IHsgTWF0VGFibGVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGbGV4TGF5b3V0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZmxleC1sYXlvdXQnO1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IEJhc2VQcm9qZWN0Q29tcG9uZW50fSBmcm9tICcuL2Jhc2UtcHJvamVjdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE1hdFRvb2x0aXBNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90b29sdGlwJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czpcbiAgICBbXG4gICAgICBDb21tb25Nb2R1bGUsXG4gICAgICBGb3Jtc01vZHVsZSxcbiAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgICBNYXRDYXJkTW9kdWxlLFxuICAgICAgTWF0SW5wdXRNb2R1bGUsXG4gICAgICBNYXRGb3JtRmllbGRNb2R1bGUsXG4gICAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgICBNYXREaXZpZGVyTW9kdWxlLFxuICAgICAgTWF0TGlzdE1vZHVsZSxcbiAgICAgIE1hdFN0ZXBwZXJNb2R1bGUsXG4gICAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgICAgTWF0SWNvbk1vZHVsZSxcbiAgICAgIE1hdFNlbGVjdE1vZHVsZSxcbiAgICAgIE1hdERhdGVwaWNrZXJNb2R1bGUsXG4gICAgICBNYXRTbmFja0Jhck1vZHVsZSxcbiAgICAgIE1hdFRhYmxlTW9kdWxlLFxuICAgICAgTWF0UGFnaW5hdG9yTW9kdWxlLFxuICAgICAgRmxleExheW91dE1vZHVsZSxcbiAgICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgICBNYXRUb29sdGlwTW9kdWxlLFxuICAgICAgUm91dGVyTW9kdWxlLmZvckNoaWxkKFtcbiAgICAgICAge1xuICAgICAgICAgIHBhdGg6ICcnLCBwYXRoTWF0Y2g6ICdmdWxsJywgY29tcG9uZW50OiBCYXNlUHJvamVjdENvbXBvbmVudFxuICAgICAgICB9XG4gICAgICBdKVxuICAgIF0sXG4gIGRlY2xhcmF0aW9uczpcbiAgICBbXG4gICAgICBCYXNlUHJvamVjdENvbXBvbmVudFxuICAgIF0sXG4gIHByb3ZpZGVyczpcbiAgICBbe1xuICAgICAgcHJvdmlkZTogJ2Jhc2UtcHJvamVjdCcsXG4gICAgICB1c2VWYWx1ZTogQmFzZVByb2plY3RDb21wb25lbnRcbiAgICB9XSxcbiAgZXhwb3J0czpcbiAgICBbXG4gICAgICBCYXNlUHJvamVjdENvbXBvbmVudFxuICAgIF0sXG4gIGVudHJ5Q29tcG9uZW50czogW1xuICAgIEJhc2VQcm9qZWN0Q29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQmFzZVByb2plY3RNb2R1bGUgeyB9XG4iXX0=