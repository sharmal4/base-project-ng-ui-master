import { ɵɵdefineComponent, ɵɵelementStart, ɵɵtext, ɵɵelementEnd, ɵsetClassMetadata, Component, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { MatCard, MatCardHeader, MatCardTitle, MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';

class BaseProjectComponent {
}
BaseProjectComponent.ɵfac = function BaseProjectComponent_Factory(t) { return new (t || BaseProjectComponent)(); };
BaseProjectComponent.ɵcmp = ɵɵdefineComponent({ type: BaseProjectComponent, selectors: [["lib-base-project"]], decls: 4, vars: 0, template: function BaseProjectComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "mat-card");
        ɵɵelementStart(1, "mat-card-header");
        ɵɵelementStart(2, "mat-card-title");
        ɵɵtext(3, "Hello Training Team");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
    } }, directives: [MatCard, MatCardHeader, MatCardTitle], styles: [""] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(BaseProjectComponent, [{
        type: Component,
        args: [{
                selector: 'lib-base-project',
                templateUrl: './base-project.component.html',
                styleUrls: ['./base-project.component.scss'],
            }]
    }], null, null); })();

class BaseProjectModule {
}
BaseProjectModule.ɵmod = ɵɵdefineNgModule({ type: BaseProjectModule });
BaseProjectModule.ɵinj = ɵɵdefineInjector({ factory: function BaseProjectModule_Factory(t) { return new (t || BaseProjectModule)(); }, providers: [{
            provide: 'base-project',
            useValue: BaseProjectComponent
        }], imports: [[
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MatCardModule,
            MatInputModule,
            MatFormFieldModule,
            MatButtonModule,
            MatDividerModule,
            MatListModule,
            MatStepperModule,
            MatProgressBarModule,
            MatCheckboxModule,
            MatIconModule,
            MatSelectModule,
            MatDatepickerModule,
            MatSnackBarModule,
            MatTableModule,
            MatPaginatorModule,
            FlexLayoutModule,
            HttpClientModule,
            MatTooltipModule,
            RouterModule.forChild([
                {
                    path: '', pathMatch: 'full', component: BaseProjectComponent
                }
            ])
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(BaseProjectModule, { declarations: [BaseProjectComponent], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        MatDividerModule,
        MatListModule,
        MatStepperModule,
        MatProgressBarModule,
        MatCheckboxModule,
        MatIconModule,
        MatSelectModule,
        MatDatepickerModule,
        MatSnackBarModule,
        MatTableModule,
        MatPaginatorModule,
        FlexLayoutModule,
        HttpClientModule,
        MatTooltipModule, RouterModule], exports: [BaseProjectComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(BaseProjectModule, [{
        type: NgModule,
        args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatCardModule,
                    MatInputModule,
                    MatFormFieldModule,
                    MatButtonModule,
                    MatDividerModule,
                    MatListModule,
                    MatStepperModule,
                    MatProgressBarModule,
                    MatCheckboxModule,
                    MatIconModule,
                    MatSelectModule,
                    MatDatepickerModule,
                    MatSnackBarModule,
                    MatTableModule,
                    MatPaginatorModule,
                    FlexLayoutModule,
                    HttpClientModule,
                    MatTooltipModule,
                    RouterModule.forChild([
                        {
                            path: '', pathMatch: 'full', component: BaseProjectComponent
                        }
                    ])
                ],
                declarations: [
                    BaseProjectComponent
                ],
                providers: [{
                        provide: 'base-project',
                        useValue: BaseProjectComponent
                    }],
                exports: [
                    BaseProjectComponent
                ],
                entryComponents: [
                    BaseProjectComponent
                ]
            }]
    }], null, null); })();

/*
 * Public API Surface of base-project
 */

/**
 * Generated bundle index. Do not edit.
 */

export { BaseProjectComponent, BaseProjectModule };
//# sourceMappingURL=base-project.js.map
