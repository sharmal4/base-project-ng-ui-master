(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/material/card'), require('@angular/material/button'), require('@angular/material/checkbox'), require('@angular/material/datepicker'), require('@angular/material/divider'), require('@angular/material/form-field'), require('@angular/material/icon'), require('@angular/material/input'), require('@angular/material/list'), require('@angular/material/paginator'), require('@angular/material/progress-bar'), require('@angular/material/select'), require('@angular/material/snack-bar'), require('@angular/material/stepper'), require('@angular/material/table'), require('@angular/common'), require('@angular/forms'), require('@angular/flex-layout'), require('@angular/common/http'), require('@angular/router'), require('@angular/material/tooltip')) :
    typeof define === 'function' && define.amd ? define('base-project', ['exports', '@angular/core', '@angular/material/card', '@angular/material/button', '@angular/material/checkbox', '@angular/material/datepicker', '@angular/material/divider', '@angular/material/form-field', '@angular/material/icon', '@angular/material/input', '@angular/material/list', '@angular/material/paginator', '@angular/material/progress-bar', '@angular/material/select', '@angular/material/snack-bar', '@angular/material/stepper', '@angular/material/table', '@angular/common', '@angular/forms', '@angular/flex-layout', '@angular/common/http', '@angular/router', '@angular/material/tooltip'], factory) :
    (global = global || self, factory(global['base-project'] = {}, global.ng.core, global.ng.material.card, global.ng.material.button, global.ng.material.checkbox, global.ng.material.datepicker, global.ng.material.divider, global.ng.material.formField, global.ng.material.icon, global.ng.material.input, global.ng.material.list, global.ng.material.paginator, global.ng.material.progressBar, global.ng.material.select, global.ng.material.snackBar, global.ng.material.stepper, global.ng.material.table, global.ng.common, global.ng.forms, global.ng.flexLayout, global.ng.common.http, global.ng.router, global.ng.material.tooltip));
}(this, (function (exports, i0, i1, button, checkbox, datepicker, divider, formField, icon, input, list, paginator, progressBar, select, snackBar, stepper, table, common, forms, flexLayout, http, i1$1, tooltip) { 'use strict';

    var BaseProjectComponent = /** @class */ (function () {
        function BaseProjectComponent() {
        }
        return BaseProjectComponent;
    }());
    BaseProjectComponent.ɵfac = function BaseProjectComponent_Factory(t) { return new (t || BaseProjectComponent)(); };
    BaseProjectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: BaseProjectComponent, selectors: [["lib-base-project"]], decls: 4, vars: 0, template: function BaseProjectComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "mat-card");
                i0.ɵɵelementStart(1, "mat-card-header");
                i0.ɵɵelementStart(2, "mat-card-title");
                i0.ɵɵtext(3, "Hello Training Team");
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
            }
        }, directives: [i1.MatCard, i1.MatCardHeader, i1.MatCardTitle], styles: [""] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(BaseProjectComponent, [{
                type: i0.Component,
                args: [{
                        selector: 'lib-base-project',
                        templateUrl: './base-project.component.html',
                        styleUrls: ['./base-project.component.scss'],
                    }]
            }], null, null);
    })();

    var BaseProjectModule = /** @class */ (function () {
        function BaseProjectModule() {
        }
        return BaseProjectModule;
    }());
    BaseProjectModule.ɵmod = i0.ɵɵdefineNgModule({ type: BaseProjectModule });
    BaseProjectModule.ɵinj = i0.ɵɵdefineInjector({ factory: function BaseProjectModule_Factory(t) { return new (t || BaseProjectModule)(); }, providers: [{
                provide: 'base-project',
                useValue: BaseProjectComponent
            }], imports: [[
                common.CommonModule,
                forms.FormsModule,
                forms.ReactiveFormsModule,
                i1.MatCardModule,
                input.MatInputModule,
                formField.MatFormFieldModule,
                button.MatButtonModule,
                divider.MatDividerModule,
                list.MatListModule,
                stepper.MatStepperModule,
                progressBar.MatProgressBarModule,
                checkbox.MatCheckboxModule,
                icon.MatIconModule,
                select.MatSelectModule,
                datepicker.MatDatepickerModule,
                snackBar.MatSnackBarModule,
                table.MatTableModule,
                paginator.MatPaginatorModule,
                flexLayout.FlexLayoutModule,
                http.HttpClientModule,
                tooltip.MatTooltipModule,
                i1$1.RouterModule.forChild([
                    {
                        path: '', pathMatch: 'full', component: BaseProjectComponent
                    }
                ])
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(BaseProjectModule, { declarations: [BaseProjectComponent], imports: [common.CommonModule,
                forms.FormsModule,
                forms.ReactiveFormsModule,
                i1.MatCardModule,
                input.MatInputModule,
                formField.MatFormFieldModule,
                button.MatButtonModule,
                divider.MatDividerModule,
                list.MatListModule,
                stepper.MatStepperModule,
                progressBar.MatProgressBarModule,
                checkbox.MatCheckboxModule,
                icon.MatIconModule,
                select.MatSelectModule,
                datepicker.MatDatepickerModule,
                snackBar.MatSnackBarModule,
                table.MatTableModule,
                paginator.MatPaginatorModule,
                flexLayout.FlexLayoutModule,
                http.HttpClientModule,
                tooltip.MatTooltipModule, i1$1.RouterModule], exports: [BaseProjectComponent] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(BaseProjectModule, [{
                type: i0.NgModule,
                args: [{
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            i1.MatCardModule,
                            input.MatInputModule,
                            formField.MatFormFieldModule,
                            button.MatButtonModule,
                            divider.MatDividerModule,
                            list.MatListModule,
                            stepper.MatStepperModule,
                            progressBar.MatProgressBarModule,
                            checkbox.MatCheckboxModule,
                            icon.MatIconModule,
                            select.MatSelectModule,
                            datepicker.MatDatepickerModule,
                            snackBar.MatSnackBarModule,
                            table.MatTableModule,
                            paginator.MatPaginatorModule,
                            flexLayout.FlexLayoutModule,
                            http.HttpClientModule,
                            tooltip.MatTooltipModule,
                            i1$1.RouterModule.forChild([
                                {
                                    path: '', pathMatch: 'full', component: BaseProjectComponent
                                }
                            ])
                        ],
                        declarations: [
                            BaseProjectComponent
                        ],
                        providers: [{
                                provide: 'base-project',
                                useValue: BaseProjectComponent
                            }],
                        exports: [
                            BaseProjectComponent
                        ],
                        entryComponents: [
                            BaseProjectComponent
                        ]
                    }]
            }], null, null);
    })();

    /*
     * Public API Surface of base-project
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.BaseProjectComponent = BaseProjectComponent;
    exports.BaseProjectModule = BaseProjectModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=base-project.umd.js.map
