import * as i0 from "@angular/core";
import * as i1 from "./base-project.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/material/card";
import * as i5 from "@angular/material/input";
import * as i6 from "@angular/material/form-field";
import * as i7 from "@angular/material/button";
import * as i8 from "@angular/material/divider";
import * as i9 from "@angular/material/list";
import * as i10 from "@angular/material/stepper";
import * as i11 from "@angular/material/progress-bar";
import * as i12 from "@angular/material/checkbox";
import * as i13 from "@angular/material/icon";
import * as i14 from "@angular/material/select";
import * as i15 from "@angular/material/datepicker";
import * as i16 from "@angular/material/snack-bar";
import * as i17 from "@angular/material/table";
import * as i18 from "@angular/material/paginator";
import * as i19 from "@angular/flex-layout";
import * as i20 from "@angular/common/http";
import * as i21 from "@angular/material/tooltip";
import * as i22 from "@angular/router";
export declare class BaseProjectModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<BaseProjectModule, [typeof i1.BaseProjectComponent], [typeof i2.CommonModule, typeof i3.FormsModule, typeof i3.ReactiveFormsModule, typeof i4.MatCardModule, typeof i5.MatInputModule, typeof i6.MatFormFieldModule, typeof i7.MatButtonModule, typeof i8.MatDividerModule, typeof i9.MatListModule, typeof i10.MatStepperModule, typeof i11.MatProgressBarModule, typeof i12.MatCheckboxModule, typeof i13.MatIconModule, typeof i14.MatSelectModule, typeof i15.MatDatepickerModule, typeof i16.MatSnackBarModule, typeof i17.MatTableModule, typeof i18.MatPaginatorModule, typeof i19.FlexLayoutModule, typeof i20.HttpClientModule, typeof i21.MatTooltipModule, typeof i22.RouterModule], [typeof i1.BaseProjectComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<BaseProjectModule>;
}
